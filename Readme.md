# hashsuite

For the times when you wish to generate one or more types of hashes for one or more types of file.


## Introduction

### Motivation

I often see people downloading files and not creating checksum with which to perform integrity checks.
This tool allows people on any platform which has Python3 installed to create checksums for algorithms provided by hashlib. Note, this is likely machine dependent.

The simplistic nature of this tool means that it does not need the user to install any other packages to work, nor does it do anything particularly fancy.


### Why not use `sha256sum` etc on Linux?

This tool doesn't aim to replace existing tooling.
It is aimed to suppliment it for all platform which has Python3 installed.

If you wish to hash files on a non-Linux system, this tool makes it easy to do.
It also reads the file(s) to hash in chunks and applies the algorithms therefore it should be fairly quick to create a suite of hashes for one or more files.


## Usage

Execute the following command to see the help/ usage text.

```
python3 hashsuite --help
```

```
usage: hashsuite [-h] [--all] [--md4] [--md5] [--sha512] [--sha3_384]
                 [--blake2s] [--sha384] [--sha3_224] [--sha224] [--sha1]
                 [--sha512_224] [--sha256] [--ripemd160] [--whirlpool]
                 [--shake_256] [--shake_128] [--blake2b] [--sha3_512] [--sm3]
                 [--md5-sha1] [--sha3_256] [--sha512_256]
                 [--alternative-format]
                 filename [filename ...]

Hash the supplied filenames, using the supplied algorithms.

Example usage:

    # hash two files using sha1
    python3 hashsuite --sha1 myfile1 myfile2

    # hash two files using sha1 and md5
    python3 hashsuite --sha1 --md5 myfile1 myfile2

    # hash three files using all of the hashing algorithms
    python3 hashsuite --all myfile1 myfile2 myfile3

positional arguments:
  filename              file(s) to which to apply the hashing algorithm(s)

optional arguments:
  -h, --help            show this help message and exit
  --all                 select all hashing algorithms
  --md4                 md4 algorithm
  --md5                 md5 algorithm
  --sha512              sha512 algorithm
  --sha3_384            sha3_384 algorithm
  --blake2s             blake2s algorithm
  --sha384              sha384 algorithm
  --sha3_224            sha3_224 algorithm
  --sha224              sha224 algorithm
  --sha1                sha1 algorithm
  --sha512_224          sha512_224 algorithm
  --sha256              sha256 algorithm
  --ripemd160           ripemd160 algorithm
  --whirlpool           whirlpool algorithm
  --shake_256           shake_256 algorithm
  --shake_128           shake_128 algorithm
  --blake2b             blake2b algorithm
  --sha3_512            sha3_512 algorithm
  --sm3                 sm3 algorithm
  --md5-sha1            md5-sha1 algorithm
  --sha3_256            sha3_256 algorithm
  --sha512_256          sha512_256 algorithm
  --alternative-format  print the hashes in alternative format
```

### Example usage

The output will look like this:


#### sha1 and md5 for two files

```
python3 hashsuite ./hashsuite ./.hashsuite.swp --sha1 --md5
```

Output:

```
./hashsuite
- md5  a47fc42aebf9d90a363fffb393742260
- sha1 f39c3d5a48ddf4aa82670da8ace99b545a47fb0f

./.hashsuite.swp
- md5  7eaf33f9c8665631d60e32cd7a3f0f27
- sha1 af36f8ef463131fd22e779bbdf1beb42963cc562
```


#### sha1 and md5 for two files in an alternative format

```
python3 hashsuite ./hashsuite ./.hashsuite.swp --sha1 --md5 --alternative-format
```

Output:

```
./hashsuite md5  a47fc42aebf9d90a363fffb393742260
./hashsuite sha1 f39c3d5a48ddf4aa82670da8ace99b545a47fb0f

./.hashsuite.swp md5  7eaf33f9c8665631d60e32cd7a3f0f27
./.hashsuite.swp sha1 af36f8ef463131fd22e779bbdf1beb42963cc562
```


#### all hash algorithms for two files

```
python3 hashsuite ./hashsuite ./.hashsuite.swp --all
```

Output:

```
./hashsuite
- md5      a47fc42aebf9d90a363fffb393742260
- sha1     f39c3d5a48ddf4aa82670da8ace99b545a47fb0f
- sha224   65f9c9fd1e1f3f7a09253cc85786d3958700991c2faf9c6918b3a548
- sha256   bdf0098fc7f419476623fcad022330d6b4cc1355a2b34e45e7c0c8f62a484e61
- sha384   a379d90ef9c3cd09e9ff3f8cb9a317335fee5cacc9df8d6716615c7e0110bb7c75fd17074d8d2d70e3a9ba6d6074ce1d
- sha512   507b50c90c68458f239c4aae659f78c18ef852ff000fd6d6cd0c04482b96a3ef11a0ed12d83bdfd4ab0e0f1974cdf16008c32663f6394131dc7bc5a0fdf2eba0
- sha3_224 5378b67d5a43a1045d4299d6c62f9e4ebbcedc77b85b06cd77e69672
- sha3_256 5d425ec9bd43141e30901ce72fae50967eccfedf25afeeb53f34befe46c8e589
- sha3_384 cded6d2cbc2a370d48e117a25d5ffec764de775b8b2c934ababf2f84d7b3021319a7bda0f26cb23cd14e675d32e7e174
- sha3_512 f94c560875b7d48c03616f33f098729abc26517a81cd6abda9a350085f9d975950da0ea2b9aeba4dd2b9203314ac88ce1bc388448f4dd8ea77df628fd4af178c

./.hashsuite.swp
- md5      0cea6b73680bbe9b9571f8ce333e894f
- sha1     f9f317731e34edfa500ec5572c11a947f49c7b50
- sha224   a83bbd307b0363496aec5abdf2ef701ca39b5eafccea67c6c67d944e
- sha256   5dcd4b62f0b7ec6b8e044dfbd9ca10f85f8f7743c30754e735edd8bee17291ba
- sha384   efc3519b1bcfca564ddb1b48bf7f91b13dfafa4f67e952361191ebecadee7407b08abe3f368c67bef248b4b2f21db985
- sha512   73fbcd267ca2318f4b4d7168ad7326af66105c1edd5e2d2d4944b0c888771522378f58e2e49b596128bfa751fa1ca5bbe31721388e39522a17bf13489838436e
- sha3_224 9b9fd6b8c2e59f860505c5d4cb08590b8613e48c85f58250ce0ddcee
- sha3_256 ef6c2d7474ef7ad8063f7eaef2765a7ddc45e6b72d211ca8aa849a32ff14d3cb
- sha3_384 b9c984c2956827d07f61a123e1179edaf4672a5ec9e5b5eadc6c37332bf749b9598281bf274ae7e89ab7c70ef5a891a8
- sha3_512 60f077148b860b3df442f6549e742a96a769c528dc0cec99073570dcd616b9eb3e8ec97e42dbb6a9f7dc942698a09662bc6fec37f5a08c1df3c734960caa1fef
```


#### all hash algorithms for two files in an alternative format

```
python3 hashsuite ./hashsuite ./.hashsuite.swp --all --alternative-format
```

Output:

```
./hashsuite md5      a47fc42aebf9d90a363fffb393742260
./hashsuite sha1     f39c3d5a48ddf4aa82670da8ace99b545a47fb0f
./hashsuite sha224   65f9c9fd1e1f3f7a09253cc85786d3958700991c2faf9c6918b3a548
./hashsuite sha256   bdf0098fc7f419476623fcad022330d6b4cc1355a2b34e45e7c0c8f62a484e61
./hashsuite sha384   a379d90ef9c3cd09e9ff3f8cb9a317335fee5cacc9df8d6716615c7e0110bb7c75fd17074d8d2d70e3a9ba6d6074ce1d
./hashsuite sha512   507b50c90c68458f239c4aae659f78c18ef852ff000fd6d6cd0c04482b96a3ef11a0ed12d83bdfd4ab0e0f1974cdf16008c32663f6394131dc7bc5a0fdf2eba0
./hashsuite sha3_224 5378b67d5a43a1045d4299d6c62f9e4ebbcedc77b85b06cd77e69672
./hashsuite sha3_256 5d425ec9bd43141e30901ce72fae50967eccfedf25afeeb53f34befe46c8e589
./hashsuite sha3_384 cded6d2cbc2a370d48e117a25d5ffec764de775b8b2c934ababf2f84d7b3021319a7bda0f26cb23cd14e675d32e7e174
./hashsuite sha3_512 f94c560875b7d48c03616f33f098729abc26517a81cd6abda9a350085f9d975950da0ea2b9aeba4dd2b9203314ac88ce1bc388448f4dd8ea77df628fd4af178c

./.hashsuite.swp md5      7eaf33f9c8665631d60e32cd7a3f0f27
./.hashsuite.swp sha1     af36f8ef463131fd22e779bbdf1beb42963cc562
./.hashsuite.swp sha224   dd5fba3bb450f0a315f5f3e5fb145b6d6906b85debce8960edd44974
./.hashsuite.swp sha256   cc5b6114ec8c2c8726190dbf6e0033dd51fbb98c74c4410ebaed1d2a5502cbd1
./.hashsuite.swp sha384   d0646f5deb1772f5b383e06099bd3b79a8e87fbf45a41b13a700a9a0efb476f51b80a9a8fc715683f8b76ff760a91800
./.hashsuite.swp sha512   f22beac55cf1c05a647edcc829d8773a3ef2e37c495d8eed274a66a0b61a7d1efd05e1cb45e76525186e639fc980bd71483d6fca32910c7774556fd6db4a07a5
./.hashsuite.swp sha3_224 39765acd53e12a48060c9e66eb523b60d82dda01c6d2bded6aa69e4c
./.hashsuite.swp sha3_256 f9bd6f2a0c24e6523bdc57d7e82b86a394d3652cb15cd3f4d67730f8caaeed44
./.hashsuite.swp sha3_384 02ec34ede7a19b6bdd11203bd7d329297f7de7c993bc9396cb3c864f9b1fc74882b15b8e98c55e95446e0a2772d796ba
./.hashsuite.swp sha3_512 9e96ef1a0559229601a0b12a39f92712103890a5f0012ba6ae74f35342c9380989eab79a55386368a8a4285623cb7cde3951467116cb3d66220645ea869f48c3
```
